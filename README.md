
# LIS4381 FALL15 A5
## Cuong Huynh

==============================================

## Requirements

*    Create remote a5 repo in your Bitbucket account


*    cd to local repos subdirectory: Example: cd c:/webdev/repos


*    Clone assignment starter files: git clone https://bitbucket.org/mjowett/a5_student_files a5 (clones a5_student_files Bitbucket repo into a5 local directory)


*    Review sub-directories, README.md and other files, and modify them accordingly a. See screenshots below.
b. Use git to push *all* a5 files and changes to your remote lis4381 Bitbucket repo
c. To get navigation links to work, modify subdirectory names accordingly.


*    Provide me read-only access to your a5 Bitbucket repo


*    Include a working link to your assignment at your Web host in the README.md file.


*    Include links to your Bitbucket repo, and Web host assignment in Blackboard.


*    Upload a5 files to your Web host


==============================================


## Links
[Bitbucket Repo](https://cnh14e@bitbucket.org/cnh14e/lis4381_fall15_a5)  
[Webhost](http://cuongnhuynh.com/repos/a5/index.php)  


